use aws_config::load_from_env;
use aws_sdk_dynamodb::{model::AttributeValue, Client};
use lambda_runtime::{handler_fn, Context, Error};
use serde::Deserialize;
use serde_json::{json, Value};
use chrono::Utc;

#[derive(Deserialize)]
struct Request {
    user: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(func);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn func(event: Value, _: Context) -> Result<Value, Error> {
    let request: Request = serde_json::from_value(event)?;
    let config = load_from_env().await;
    let client = Client::new(&config);

    store_username(&client, &request.user).await?;
    Ok(
        json!({ "message": format!("Username '{}' has been stored successfully.", request.user) }),
    )
}

async fn store_username(client: &Client, user: &str) -> Result<(), Error> {
    let table_name = std::env::var("TABLE_NAME").expect("TABLE_NAME must be set");
    let timestamp = Utc::now().to_rfc3339();

    client
        .put_item()
        .table_name(&table_name)
        .item("user", AttributeValue::S(user.to_string()))
        .item("timestamp", AttributeValue::S(timestamp))
        .send()
        .await?;

    Ok(())
}
