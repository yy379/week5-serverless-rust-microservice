# Week5 Serverless Rust Microservice

## Requirements
- Create a Rust AWS Lambda function (or app runner)
- Implement a simple service
- Connect to a database

## Steps to run the project
### Use Cargo to init the project
```bash
cargo init
```

### Add components to main.rs, Cargo.toml, and other files
- In this project, I managed to store username in DynamoDB

### Build the project
```bash
cargo build
```

### Create a new table in DynamoDB
![DynamoDB](./screenshots/create_table.png)
### Deploy the project
```bash
cargo lambda deploy --region 
```
![Deploy](./screenshots/deploy.png)
![Deploy](./screenshots/db.png)
### Test the project
- Use trigger or API Gateway to test the project
![Test](./screenshots/lambda.png)
## Cargo commands
- `cargo build`: Compiles your project
- `cargo run`: Compiles and runs your project
- `cargo test`: Runs the tests
- `cargo doc`: Builds the documentation
- `cargo update`: Updates your dependencies
- `cargo check`: Checks your code to make sure it compiles but doesn't produce an executable
- `cargo clean`: Removes the target directory

